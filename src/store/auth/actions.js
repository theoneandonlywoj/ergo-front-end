import { LocalStorage, Loading, Notify } from 'quasar'
import { ergoBackend } from '../../boot/axios'

export function registerUserAction ({ commit }, payload) {
  Loading.show()
  ergoBackend({
    method: 'post',
    url: '/users',
    data: {
      user: {
        name: payload.fullName,
        email: payload.email,
        password: payload.password
      }
    }
  }).then(() => {
    Notify.create({
      color: 'positive',
      position: 'top',
      message: 'User Registered!',
      icon: 'check_box'
    })
    Loading.hide()
  })
    .catch((err) => {
      const responseCode = err.response.status
      LocalStorage.set('loggedIn', false)
      Loading.hide()
      let errorMessage = ''
      if (responseCode === 422) {
        errorMessage = 'Could not create the User! It probably already exists!'
      } else {
        errorMessage = JSON.stringify(err.message)
      }
      Notify.create({
        color: 'negative',
        position: 'top',
        message: errorMessage,
        icon: 'report_problem'
      })
    })
}

export function loginUserAction ({ commit }, payload) {
  Loading.show()
  ergoBackend({
    method: 'post',
    url: '/sign_in',
    data: {
      email: payload.email,
      password: payload.password
    }
  }).then(() => {
    commit('setLoggedInMutation', true)
    LocalStorage.set('loggedIn', true)
    Loading.hide()
  })
    .catch((err) => {
      Loading.hide()
      let responseCode = ''
      if (err.response) {
        responseCode = err.response.status
      }
      LocalStorage.set('loggedIn', false)
      let errorMessage = ''
      if (responseCode === 401) {
        errorMessage = 'User or Password are incorrect!'
      } else {
        errorMessage = JSON.stringify(err.message)
      }
      Notify.create({
        color: 'negative',
        position: 'top',
        message: errorMessage,
        icon: 'report_problem'
      })
    })
}

export function logoutUserAction ({ commit }) {
  Loading.show()
  setTimeout(
    () => {
      commit('setLoggedInMutation', false)
      LocalStorage.set('loggedIn', false)
      Loading.hide()
    }, 1000)
}
