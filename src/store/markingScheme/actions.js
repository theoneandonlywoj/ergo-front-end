import { Notify } from 'quasar'
import { ergoBackend } from '../../boot/axios'

export function setNextModeAction ({ commit }, value) {
  commit('setNextModeMutation')
}

export function setPreviousModeAction ({ commit }, value) {
  commit('setPreviousModeMutation')
}

export function setChosenCohortAction ({ commit }, value) {
  commit('setChosenCohortMutation', value)
}

export function setChosenLearnerAction ({ commit }, value) {
  commit('setChosenLearnerMutation', value)
}

export function setChosenMarkerAction ({ commit }, value) {
  commit('setChosenMarkerMutation', value)
}

export function setChosenModuleAction ({ commit, dispatch }, value) {
  commit('setChosenModuleMutation', value)
  dispatch('getFeedbackListAction', value)
}

export function setAllModuleRequirementsTableAction ({ commit }, value) {
  commit('setAllModuleRequirementsTableMutation', value)
}

export function setSelectedWellDoneAction ({ commit }, value) {
  commit('setSelectedWellDoneMutation', value)
}

export function setSelectedNeedsCorrectionsAction ({ commit }, value) {
  commit('setSelectedNeedsCorrectionsMutation', value)
}

export function setSectionsForModuleAction ({ commit }, value) {
  commit('setSectionsForModuleMutation', value)
}

export function setCommentsAction ({ commit }, value) {
  commit('setCommentsMutation', value)
}

export function setShowAddRequirementDialogAction ({ commit }, value) {
  commit('setShowAddRequirementDialogMutation', value)
}

export function setChosenProjectSectionAction ({ commit }, value) {
  commit('setChosenProjectSectionMutation', value)
}

export function getFeedbackListAction ({ commit }, module) {
  if (module) {
    ergoBackend({
      method: 'get',
      url: `/feedback_for_module_id?module_id=${module.id}`
    }).then((response) => {
      commit('setFeedbackListMutation', response.data.data)
    })
      .catch((err) => {
        Notify.create({
          color: 'negative',
          position: 'top',
          message: JSON.stringify(err.message),
          icon: 'report_problem'
        })
      })
  } else {
    commit('resetFeedbackListMutation')
  }
}

export function getProjectSectionsForModuleAction ({ commit }, module) {
  if (module) {
    ergoBackend({
      method: 'get',
      url: `/project_sections_for_module_id?module_id=${module.id}`
    }).then((response) => {
      commit('setSectionsForModuleMutation', response.data.data)
    })
      .catch((err) => {
        Notify.create({
          color: 'negative',
          position: 'top',
          message: JSON.stringify(err.message),
          icon: 'report_problem'
        })
      })
  } else {
    commit('resetFeedbackListMutation')
  }
}
