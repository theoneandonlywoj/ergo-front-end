export function modeGetter (state) {
  return state.mode
}

export function chosenCohortGetter (state) {
  return state.chosenCohort
}

export function chosenCohortIdGetter (state) {
  return state.chosenCohort.id
}

export function chosenLearnerGetter (state) {
  return state.chosenLearner
}

export function chosenMarkerGetter (state) {
  return state.chosenMarker
}

export function chosenModuleGetter (state) {
  return state.chosenModule
}

export function allModuleRequirementsTableGetter (state) {
  return state.allModuleRequirementsTable
}

export function selectedWellDoneGetter (state) {
  return state.selectedWellDone
}

export function selectedNeedsCorrectionsGetter (state) {
  return state.selectedNeedsCorrections
}

export function sectionsForModuleGetter (state) {
  return state.sectionsForModule
}

export function commentsGetter (state) {
  return state.comments
}

export function showAddRequirementDialogGetter (state) {
  return state.showAddRequirementDialog
}

export function chosenProjectSectionGetter (state) {
  return state.chosenProjectSection
}

export function feedbackListGetter (state) {
  return state.feedbackList
}
