export default function () {
  return {
    mode: 'marking',
    chosenCohort: null,
    chosenLearner: null,
    chosenMarker: null,
    chosenModule: null,
    chosenProjectSection: null,
    allModuleRequirementsTable: [],
    selectedWellDone: [],
    selectedNeedsCorrections: [],
    comments: '',
    sectionsForModule: [],
    showAddRequirementDialog: false,
    feedbackList: []
  }
}
