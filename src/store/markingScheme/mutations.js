export function setNextModeMutation (state) {
  if (state.mode === 'marking') {
    state.mode = 'pre-rendering'
  } else if (state.mode === 'pre-rendering') {
    state.mode = 'rendering'
  }
}

export function setPreviousModeMutation (state) {
  if (state.mode === 'pre-rendering') {
    state.mode = 'marking'
  } else if (state.mode === 'rendering') {
    state.mode = 'pre-rendering'
  }
}

export function setChosenCohortMutation (state, value) {
  state.chosenCohort = value
}

export function setChosenLearnerMutation (state, value) {
  state.chosenLearner = value
}

export function setChosenMarkerMutation (state, value) {
  state.chosenMarker = value
}

export function setChosenModuleMutation (state, value) {
  state.chosenModule = value
}

export function setAllModuleRequirementsTableMutation (state, value) {
  state.allModuleRequirementsTable = value
}

export function setSelectedWellDoneMutation (state, value) {
  state.selectedWellDone = value
}

export function setSelectedNeedsCorrectionsMutation (state, value) {
  state.selectedNeedsCorrections = value
}

export function toggleSelectedWellDoneMutation (state, item) {
  const itemInSelectedNeedsCorrections = state.selectedNeedsCorrections.filter(
    tableItem => tableItem.id === item.id
  )
  if (itemInSelectedNeedsCorrections.length > 0) {
    state.selectedNeedsCorrections = state.selectedNeedsCorrections.filter(
      itemInTable => itemInTable.id !== item.id
    )
  }
  const itemInSelectedWellDone = state.selectedWellDone.filter(
    tableItem => tableItem.id === item.id
  )
  if (itemInSelectedWellDone.length > 0) {
    state.selectedWellDone = state.selectedWellDone.filter(
      itemInTable => itemInTable.id !== item.id
    )
  } else {
    state.selectedWellDone = state.selectedWellDone.concat([item])
  }
}

export function toggleNeedsCorrectionsMutation (state, item) {
  const itemInSelectedWellDone = state.selectedWellDone.filter(
    tableItem => tableItem.id === item.id
  )
  if (itemInSelectedWellDone.length > 0) {
    state.selectedWellDone = state.selectedWellDone.filter(
      itemInTable => itemInTable.id !== item.id
    )
  }
  const itemInSelectedNeedsCorrections = state.selectedNeedsCorrections.filter(
    tableItem => tableItem.id === item.id
  )
  if (itemInSelectedNeedsCorrections.length > 0) {
    state.selectedNeedsCorrections = state.selectedNeedsCorrections.filter(
      itemInTable => itemInTable.id !== item.id
    )
  } else {
    state.selectedNeedsCorrections = state.selectedNeedsCorrections.concat([
      item
    ])
  }
}

export function setSectionsForModuleMutation (state, value) {
  state.sectionsForModule = value
}

export function setCommentsMutation (state, value) {
  state.comments = value
}

export function setShowAddRequirementDialogMutation (state, value) {
  state.showAddRequirementDialog = value
}

export function setChosenProjectSectionMutation (state, value) {
  state.chosenProjectSection = value
}

export function setFeedbackListMutation (state, value) {
  state.feedbackList = value
}

export function resetFeedbackListMutation (state) {
  state.feedbackList = []
}
