import Vue from 'vue'
import axios from 'axios'

const ergoBackend = axios.create({
  baseURL: `${process.env.VUE_APP_BACKEND_ENDPOINT}`,
  timeout: 1000
})

Vue.prototype.$axios = ergoBackend

export { ergoBackend }
