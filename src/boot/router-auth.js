import { LocalStorage } from 'quasar'
import store from '../store'

export default ({ router }) => {
  router.beforeEach((to, from, next) => {
    const loggedIn = LocalStorage.getItem('loggedIn')
    store().commit('auth/setLoggedInMutation', loggedIn)
    if (!loggedIn && to.name !== 'Auth') {
      next({ name: 'Auth' })
    } else {
      next()
    }
  })
}
