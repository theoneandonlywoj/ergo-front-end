# Ergo by Decoded (ergo-front-end)

Ergo

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Docker
##### Build
```bash
sudo docker build -t ergo_frontend .
```

##### Run (interactively)
```bash
sudo docker run -it -p 8000:80 --rm ergo_frontend
```
### Live setup
Live setup doesn't use Docker as of now!
Use for test purposes for now.

### Adding DotEnv
```bash
quasar ext add @quasar/dotenv
```